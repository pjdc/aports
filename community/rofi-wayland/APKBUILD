# Contributor: Marvin Preuss <marvin@xsteadfastx.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=rofi-wayland
_projname=rofi
pkgver=1.7.1_p1
_pkgver="${pkgver%_p*}+wayland${pkgver#*_p}"
pkgrel=2
pkgdesc="Window switcher, run dialog and dmenu replacement - fork with wayland support"
url="https://github.com/lbonn/rofi"
arch="all !s390x !mips !mips64 !riscv64"  # limited by librsvg -> rust
license="MIT"
depends="!rofi rofi-themes>=${pkgver%.*}"
makedepends="
	bison
	cairo-dev
	flex
	gdk-pixbuf-dev
	glib-dev
	libxkbcommon-dev
	meson
	pango-dev
	ronn
	wayland-dev
	wayland-protocols
	"
checkdepends="check-dev cppcheck xkeyboard-config"
provider_priority=10  # lowest (other provider is rofi aport)
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/lbonn/rofi/releases/download/$_pkgver/rofi-$_pkgver.tar.gz
	rofi-sensible-terminal-use-sh.patch
	disable-scrollbar-test.patch
	"
builddir="$srcdir/$_projname-$_pkgver"

build() {
	abuild-meson \
		-Dxcb=disabled \
		-Dcheck=disabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	# Themes are provided by package rofi-themes from the rofi aport.
	rm -rf "$pkgdir"/usr/share/rofi/themes
}

sha512sums="
daab65dacd66e949a4ba436c1bf531097d1b8d7fa4d885c01ad6e09d9bbf6c23d625972312596b8047bfd8aa4b7077e4dc4f003abfca41b3b0628d7901dcafa3  rofi-1.7.1+wayland1.tar.gz
13a93621337b4f317031da9007ed9d426b055190fc946a87eb12333469f9a23e85763e2c1e0492ff2d1d50ceebd429d8125a0b275d1ab0573a4d10cd812bccd9  rofi-sensible-terminal-use-sh.patch
e00b6ad74ed482ce5a77e7311ad6797481440825006fded5676b5417548bc93b4baa39b80f0f86e645d804dd2e606a894384d7974e10739193561c56af6343f1  disable-scrollbar-test.patch
"
