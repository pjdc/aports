# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=chezmoi
pkgver=2.9.2
pkgrel=0
pkgdesc="Manage your dotfiles across multiple machines, securely."
url="https://www.chezmoi.io/"
arch="all"
license="MIT"
makedepends="go"
options="!check chmod-clean" # no test suite
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/twpayne/chezmoi/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	go build \
		-ldflags "
		-X \"main.version=$pkgver\" \
		-X \"main.date=$(date --utc +%Y-%m-%dT%H:%M:%SZ)\" \
		-X \"github.com/twpayne/chezmoi/cmd.DocsDir=/usr/share/doc/chezmoi/\"
		" \
		-tags noupgrade \
		-tags noembeddocs
	make completions
}

package() {
	install -Dm0755 chezmoi "$pkgdir"/usr/bin/chezmoi
	install -Dm0644 completions/chezmoi-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm0644 completions/chezmoi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
	install -Dm0644 completions/chezmoi.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc/chezmoi"
	cp "$builddir/docs/"* "$subpkgdir/usr/share/doc/chezmoi"
}

sha512sums="
b47bf6a17f80b03151d365e0f67d24b40eea0c197ce9cc46d27c2b8e1c34dda9d4e153037509acc160799ca1eb25ea90140995bf13c64b89c3df2777ba714d01  chezmoi-2.9.2.tar.gz
"
